import React from "react";
import ReactDOM from "react-dom/client";
import Module from "./Module";
import Styled from "./Styled";
import "./index.css";
import "antd/dist/antd.css";
import AppHeader from "./components/header";
import Carousel from "./components/carousel";
import Collapse from "./components/collapse";
import Table from "./components/table";

import { DatePicker } from "antd";
import { Layout } from "antd";


const { Header } = Layout;

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <Layout className="mainLayout">
      <Header>
        <AppHeader />
      </Header>
    </Layout>
    <Carousel/>
    <Collapse/>
    <Table/>


    <div>
      <DatePicker />
    </div>

    <main>
      <h1>Hello World</h1>
    </main>

    <Module />

    <Styled />
  </React.StrictMode>
);
