import React from "react";
import { Menu } from "antd";

function AppHeader() {
  return (
	<div className = "container-fluid">
       <Menu theme= "dark" mode="horizontal" defaultSelectedKeys={["Home"]} >
          <Menu.Item key="Home">Home</Menu.Item>
          <Menu.Item key="About">About</Menu.Item>
          <Menu.Item key="features">features</Menu.Item>
        </Menu>
	</div>
    );
}
export default AppHeader;


